#![recursion_limit="512"]
#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate chrono;
extern crate bigdecimal;

use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use std::env;

mod models;
mod schema;

use models::SpreeProduct;
use schema::spree_products::dsl::*;

fn main() {
    dotenv().ok();
    let data_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    let connection = PgConnection::establish(&data_url)
        .expect(&format!("Error connect to {}", data_url));

    let results = spree_products.limit(2)
        .load::<SpreeProduct>(&connection)
        .expect("Error loading products");

    for product in results {
        print!("{} {} \n", product.id, product.name);
    }
}
